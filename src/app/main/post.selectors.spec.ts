import * as PostSelectors from './post.selectors';
import { initialState } from './store/post.reducer';

describe('Post selectors', () => {
  const appState = {
    post: initialState,
  };

  it('should select loading false', () => {
    const loading = PostSelectors.postLoadingSelector(appState);
    expect(loading).toBe(false);
  });

  it('should select editing false', () => {
    const editing = PostSelectors.postEditingSelector(appState);
    expect(editing).toBe(false);
  });

  it('should select edited post', () => {
    const errorMessage = 'error lol';
    const error = PostSelectors.postErrorSelector({ post: { errorMessage } });
    expect(error).toBe(errorMessage);
  });

  it('should select edited post', () => {
    const errorMessage = 'error lol';
    const error = PostSelectors.postErrorSelector({ post: { errorMessage } });
    expect(error).toBe(errorMessage);
  });

  it('should select posts', () => {
    const posts = PostSelectors.postsSelector({ post: { posts: [{ title: '1' }, { title: '2' }] } });
    expect(posts.length).toBe(2);
  });
});
