import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MainService } from './service/main.service';
import { Post } from '../main/post.model';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnInit, OnDestroy {
  posts$ = this._mainService.posts$;
  errorMessage$ = this._mainService.msgObservable$;
  isLoading$ = this._mainService.laodingObservable$;
  isEditing = false;
  editedPostID = '';

  private readonly subscription = new Subscription();

  constructor(private readonly _mainService: MainService) {}

  ngOnInit(): void {
    this.onGetPostsClick();
    this.subscription.add(this._mainService.editingObservable$.subscribe(value => (this.isEditing = value)));
  }

  onPostFormSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    if (this.isEditing) {
      this._mainService.updatePost(this.editedPostID, form.value.title, form.value.message);
    } else {
      this._mainService.createPost({ title: form.value.title, message: form.value.message });
    }
    form.resetForm();
  }

  onGetPostsClick() {
    this._mainService.loadPosts();
  }

  onEditPostClick(post: Post, form: NgForm) {
    form.controls.title.setValue(post.title);
    form.controls.message.setValue(post.message);
    this.editedPostID = post.id;
    this._mainService.startUpdate(post.id);
  }

  onDeletePostsClick() {
    this._mainService.deletePost();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
