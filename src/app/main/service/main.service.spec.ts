import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { MockStore } from '../../mocks/mokcs.store.spec';
import { MainService } from './main.service';
import { Post } from '../post.model';
import * as fromApp from '../../reducers/index';
import * as selectors from '../post.selectors';
import * as postActions from '../store/post.actions';

describe('MainService', () => {
  let service: MainService;
  let mockStore: MockStore<fromApp.State>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: Store, useClass: MockStore }],
    });
    service = TestBed.inject(MainService);
    mockStore = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('loadPosts', () => {
    it('should fire get posts action', () => {
      service.loadPosts();

      expect(mockStore.dispatch).toHaveBeenCalledWith(new postActions.getPostAction());
    });
  });
  describe('createPost', () => {
    it('should fire create post action', () => {
      const postData: Post = { title: 'title', message: 'message' };
      service.createPost(postData);

      expect(mockStore.dispatch).toHaveBeenCalledWith(new postActions.createPostAction(postData));
    });
  });
  describe('startUpdate', () => {
    it('should fire start post editing action', () => {
      const postId = 'postId';
      service.startUpdate(postId);

      expect(mockStore.dispatch).toHaveBeenCalledWith(new postActions.startEditAction(postId));
    });
  });

  describe('updatePost', () => {
    it('should fire update post action', () => {
      const postData: Post = { id: 'id', title: 'title', message: 'message' };
      service.updatePost(postData.id, postData.title, postData.message);

      expect(mockStore.dispatch).toHaveBeenCalledWith(new postActions.updatePostAction(postData));
    });
  });

  describe('deletePost', () => {
    it('should fire delete post action', () => {
      service.deletePost();

      expect(mockStore.dispatch).toHaveBeenCalledWith(new postActions.deletePostAction());
    });
  });
});
