import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromApp from '../../reducers/index';
import * as postActions from '../store/post.actions';
import * as selectors from '../post.selectors';

@Injectable({
  providedIn: 'root',
})
export class MainService {
  msgObservable$ = this._store.pipe(select(selectors.postErrorSelector));
  laodingObservable$ = this._store.pipe(select(selectors.postLoadingSelector));
  editingObservable$ = this._store.pipe(select(selectors.postEditingSelector));
  posts$ = this._store.pipe(select(selectors.postsSelector));

  constructor(private _store: Store<fromApp.State>) {}

  createPost(postData: { title: string; message: string }) {
    this._store.dispatch(new postActions.createPostAction(postData));
  }

  loadPosts() {
    this._store.dispatch(new postActions.getPostAction());
  }

  startUpdate(id: string) {
    this._store.dispatch(new postActions.startEditAction(id));
  }

  updatePost(id: string, title: string, message: string) {
    this._store.dispatch(new postActions.updatePostAction({ id, title, message }));
    this._store.dispatch(new postActions.endEditAction());
  }

  deletePost() {
    this._store.dispatch(new postActions.deletePostAction());
  }
}
