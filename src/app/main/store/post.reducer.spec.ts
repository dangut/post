import * as PostActions from './post.actions';
import { Post } from '../post.model';
import { initialState, postReducer, State } from './post.reducer';

describe('Post Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = postReducer(initialState, action);
      expect(result).toEqual(initialState);

      const state1 = postReducer(undefined, action);
      expect(state1).toEqual(initialState);
    });
  });

  describe('Create post actions', () => {
    it('should be loading when creating a post', () => {
      const postData: Post = { title: 'title', message: 'message' };
      const state = postReducer(initialState, new PostActions.createPostAction(postData));

      expect(state.loading).toBeTrue();
    });

    it('should add new post to state without id', () => {
      const postData: Post = { title: 'title', message: 'message' };
      const state = postReducer(initialState, new PostActions.createPostAction(postData));

      expect(state.posts).toContain(postData);
    });

    it('should stop loading on success', () => {
      const newId = '1';
      const state: State = {
        posts: [{ title: 't', message: 'm' } as Post],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.createPostSuccessAction(newId));

      expect(newState.loading).toBeFalse();
    });

    it('should set id after created the post', () => {
      const newId = '1';
      const newPost: Post = { title: 't', message: 'm' };
      const state: State = {
        posts: [newPost],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.createPostSuccessAction(newId));

      expect(newState.posts).toContain({ ...newPost, id: newId });
    });

    it('should return error on fail', () => {
      const errorMessage = 'error has happend';
      const state: State = {
        posts: [{ title: 't', message: 'm' } as Post],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.createPostFailAction(errorMessage));

      expect(newState.errorMessage).toBe(errorMessage);
    });

    it('should stop loading on fail', () => {
      const newId = '1';
      const state: State = {
        posts: [{ title: 't', message: 'm' } as Post],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.createPostFailAction(newId));

      expect(newState.loading).toBeFalse();
    });
  });

  describe('update post actions', () => {
    it('should be loading when updating a post', () => {
      const postData: Post = { id: '1', title: 'title', message: 'message' };
      const newState = postReducer(initialState, new PostActions.updatePostAction(postData));

      expect(newState.loading).toBeTrue();
    });

    it('should be in editing mode when editing', () => {
      const postData: Post = { id: '1', title: 'title', message: 'message' };
      const state: State = {
        posts: [postData],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.startEditAction('1'));

      expect(newState.editedPost).toEqual(postData);
      expect(newState.editing).toBeTrue();
    });

    it('should stop loading after success', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [post],
        loading: true,
        editing: true,
        editedPost: post,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.updatePostSuccessAction());

      expect(newState.loading).toBeFalse();
    });

    it('should stop loading after fail', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [post],
        loading: true,
        editing: true,
        editedPost: post,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.updatePostFailAction('error'));

      expect(newState.loading).toBeFalse();
    });

    it('should have error on failed update', () => {
      const errorMessage = 'error';
      const newState = postReducer(initialState, new PostActions.updatePostFailAction(errorMessage));

      expect(newState.errorMessage).toBe(errorMessage);
    });

    it('should end editing', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [post],
        loading: true,
        editing: true,
        editedPost: post,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.endEditAction());

      expect(newState.editedPost).toBeNull();
      expect(newState.editing).toBeFalse();
    });
  });

  describe('delete post actions', () => {
    it('should start loading when deleting', () => {
      const newState = postReducer(initialState, new PostActions.deletePostAction());

      expect(newState.loading).toBeTrue();
    });

    it('should stop loading when success', () => {
      const state: State = {
        posts: [],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.deletePostSuccessAction());

      expect(newState.loading).toBeFalse();
    });

    it('should stop loading when failed', () => {
      const state: State = {
        posts: [],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.deletePostFailAction('error'));

      expect(newState.loading).toBeFalse();
    });

    it('should delete posts from state', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [post],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.deletePostSuccessAction());

      expect(newState.posts.length).toBe(0);
    });

    it('should return error message on fail', () => {
      const errorMessage = 'error';
      const newState = postReducer(initialState, new PostActions.deletePostFailAction(errorMessage));

      expect(newState.errorMessage).toBe(errorMessage);
    });
  });

  describe('get post actions', () => {
    it('should start loading when getting posts', () => {
      const newState = postReducer(initialState, new PostActions.getPostAction());

      expect(newState.loading).toBeTrue();
    });

    it('should stop loading on success', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.getPostSuccessAction([post]));

      expect(newState.loading).toBeFalse();
    });

    it('should stop loading on fail', () => {
      const state: State = {
        posts: [],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.getPostFailAction('error'));

      expect(newState.loading).toBeFalse();
    });

    it('should set posts on success', () => {
      const post: Post = { id: '1', title: 't', message: 'm' };
      const state: State = {
        posts: [],
        loading: true,
        editing: false,
        editedPost: null,
        errorMessage: null,
      };
      const newState = postReducer(state, new PostActions.getPostSuccessAction([post, post, post]));

      expect(newState.posts.length).toBe(3);
    });

    it('should return error message on fail', () => {
      const errorMessage = 'error';
      const newState = postReducer(initialState, new PostActions.getPostFailAction(errorMessage));

      expect(newState.errorMessage).toBe(errorMessage);
    });
  });
});
