import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import * as PostActions from './post.actions';

@Injectable()
export class PostEffects {
  @Effect()
  public postCreate$ = this.actions$.pipe(
    ofType(PostActions.PostActionTypes.CreatePost),
    switchMap((postData: PostActions.createPostAction) => {
      return this._http.post('https://test-angular-85c05.firebaseio.com/posts.json', postData.payload).pipe(
        map(response => {
          return new PostActions.createPostSuccessAction(response['name']);
        }),
        catchError(() => {
          const errorMessage = 'An unknown error occurred!';
          return of(new PostActions.createPostFailAction(errorMessage));
        }),
      );
    }),
  );

  @Effect()
  public postUpdate$ = this.actions$.pipe(
    ofType(PostActions.PostActionTypes.UpdatePost),
    switchMap((postData: PostActions.updatePostAction) => {
      const post = {};
      post[postData.payload.id] = { title: postData.payload.title, message: postData.payload.message };
      return this._http.patch('https://test-angular-85c05.firebaseio.com/posts.json', post).pipe(
        map(() => {
          return new PostActions.updatePostSuccessAction();
        }),
        catchError(() => {
          const errorMessage = 'An unknown error occurred!';
          return of(new PostActions.updatePostFailAction(errorMessage));
        }),
      );
    }),
  );

  @Effect()
  public getPost$ = this.actions$.pipe(
    ofType(PostActions.PostActionTypes.GetPost),
    switchMap((postData: PostActions.getPostAction) => {
      return this._http.get('https://test-angular-85c05.firebaseio.com/posts.json').pipe(
        map(response => {
          const loadedPosts = [];
          for (const key in response) {
            if (response.hasOwnProperty(key)) {
              loadedPosts.push({ ...response[key], id: key });
            }
          }
          return new PostActions.getPostSuccessAction(loadedPosts);
        }),
        catchError(() => {
          const errorMessage = 'An unknown error occurred!';
          return of(new PostActions.getPostFailAction(errorMessage));
        }),
      );
    }),
  );

  @Effect()
  deletePost = this.actions$.pipe(
    ofType(PostActions.PostActionTypes.DeletePost),
    switchMap((postData: PostActions.deletePostAction) => {
      return this._http.delete('https://test-angular-85c05.firebaseio.com/posts.json').pipe(
        map(() => new PostActions.deletePostSuccessAction()),
        catchError(() => {
          const errorMessage = 'An unknown error occurred!';
          return of(new PostActions.deletePostFailAction(errorMessage));
        }),
      );
    }),
  );

  constructor(private actions$: Actions, private _http: HttpClient) {}
}
