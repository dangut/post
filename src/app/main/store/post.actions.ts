import { Action } from '@ngrx/store';
import { Post } from '../post.model';

export enum PostActionTypes {
  CreatePost = '[Post] Create post',
  CreatePostSuccess = '[Post] Create post success',
  CreatePostFail = '[Post] Create post fail',
  UpdatePost = '[Post] Update post',
  UpdatePostSuccess = '[Post] Update post success',
  UpdatePostFail = '[Post] Update post fail',
  GetPost = '[Post] Get post',
  GetPostSuccess = '[Post] Get post success',
  GetPostFail = '[Post] Get post fail',
  DeletePost = '[Post] Delete post',
  DeletePostSuccess = '[Post] Delete post success',
  DeletePostFail = '[Post] Delete post fail',
  StartEdit = '[Post] Start edit',
  EndEdit = '[Post] End Edit',
}

export class createPostAction implements Action {
  readonly type = PostActionTypes.CreatePost;

  constructor(public payload: Post) {}
}

export class createPostSuccessAction implements Action {
  readonly type = PostActionTypes.CreatePostSuccess;
  constructor(public payload: string) {}
}

export class createPostFailAction implements Action {
  readonly type = PostActionTypes.CreatePostFail;
  constructor(public payload: string) {}
}

export class updatePostAction implements Action {
  readonly type = PostActionTypes.UpdatePost;

  constructor(public payload: Post) {}
}

export class updatePostSuccessAction implements Action {
  readonly type = PostActionTypes.UpdatePostSuccess;
}

export class updatePostFailAction implements Action {
  readonly type = PostActionTypes.UpdatePostFail;
  constructor(public payload: string) {}
}

export class getPostAction implements Action {
  readonly type = PostActionTypes.GetPost;
}

export class getPostSuccessAction implements Action {
  readonly type = PostActionTypes.GetPostSuccess;
  constructor(public payload: Post[]) {}
}

export class getPostFailAction implements Action {
  readonly type = PostActionTypes.GetPostFail;
  constructor(public payload: string) {}
}

export class deletePostAction implements Action {
  readonly type = PostActionTypes.DeletePost;
}

export class deletePostSuccessAction implements Action {
  readonly type = PostActionTypes.DeletePostSuccess;
}

export class deletePostFailAction implements Action {
  readonly type = PostActionTypes.DeletePostFail;
  constructor(public payload: string) {}
}

export class startEditAction implements Action {
  readonly type = PostActionTypes.StartEdit;
  constructor(public payload: string) {}
}

export class endEditAction implements Action {
  readonly type = PostActionTypes.EndEdit;
}

export type PostActions =
  | createPostAction
  | createPostSuccessAction
  | createPostFailAction
  | updatePostAction
  | updatePostSuccessAction
  | updatePostFailAction
  | getPostAction
  | getPostSuccessAction
  | getPostFailAction
  | deletePostAction
  | deletePostSuccessAction
  | deletePostFailAction
  | startEditAction
  | endEditAction;
