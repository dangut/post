import * as PostActions from './post.actions';
import { Post } from '../post.model';

export interface State {
  posts: Post[];
  errorMessage: string;
  loading: boolean;
  editing: boolean;
  editedPost: Post;
}

export const initialState: State = {
  posts: [],
  errorMessage: null,
  loading: false,
  editing: false,
  editedPost: null,
};

export function postReducer(state = initialState, action: PostActions.PostActions) {
  switch (action.type) {
    case PostActions.PostActionTypes.CreatePost: {
      return { ...state, posts: [...state.posts, action.payload], loading: true };
    }
    case PostActions.PostActionTypes.CreatePostSuccess: {
      const createdPost = { ...state.posts[state.posts.length - 1], id: action.payload };
      const editedPosts = [...state.posts];
      editedPosts[editedPosts.length - 1] = createdPost;
      return { ...state, posts: editedPosts, errorMessage: null, loading: false };
    }
    case PostActions.PostActionTypes.CreatePostFail: {
      return { ...state, errorMessage: action.payload, loading: false };
    }

    case PostActions.PostActionTypes.UpdatePost: {
      const postToEdit = state.posts.find(post => state.editedPost.id === post.id);
      const editedPost = { ...postToEdit, ...action.payload };
      const editedPosts = [...state.posts];
      const postIndex = editedPosts.findIndex(post => state.editedPost.id === post.id);
      editedPosts[postIndex] = editedPost;
      return { ...state, posts: [...editedPosts], loading: true };
    }
    case PostActions.PostActionTypes.UpdatePostSuccess: {
      return { ...state, errorMessage: null, loading: false };
    }
    case PostActions.PostActionTypes.UpdatePostFail: {
      return { ...state, errorMessage: action.payload, loading: false };
    }

    case PostActions.PostActionTypes.GetPost: {
      return { ...state, loading: true };
    }
    case PostActions.PostActionTypes.GetPostSuccess: {
      return { ...state, posts: [...action.payload], PostserrorMessage: null, loading: false };
    }
    case PostActions.PostActionTypes.GetPostFail: {
      return { ...state, errorMessage: action.payload, loading: false };
    }

    case PostActions.PostActionTypes.DeletePost: {
      return { ...state, loading: true };
    }
    case PostActions.PostActionTypes.DeletePostSuccess: {
      return { ...state, posts: [], errorMessage: null, loading: false };
    }
    case PostActions.PostActionTypes.DeletePostFail: {
      return { ...state, errorMessage: action.payload, loading: false };
    }

    case PostActions.PostActionTypes.StartEdit: {
      const editedPost = state.posts.find(post => action.payload === post.id);
      return { ...state, editing: true, editedPost };
    }
    case PostActions.PostActionTypes.EndEdit: {
      return { ...state, editing: false, editedPost: null };
    }
    default:
      return { ...state };
  }
}
