import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { MainComponent } from './main.component';
import { NgForm, NgModel, AbstractControl, FormControl } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { MainService } from './service/main.service';
import { of } from 'rxjs';
import { Post } from './post.model';

describe('MainComponent', () => {
  const testForm = <NgForm>{
    value: {
      title: 'Hello',
      message: 'World',
    },
    resetForm() {},
  };

  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;
  let mainService: MainServiceMock;
  class MainServiceMock {
    msgObservable$ = of(null);
    laodingObservable$ = of(null);
    editingObservable$ = of(null);
    posts$ = of(null);

    public createPost = jasmine.createSpy();
    public loadPosts = jasmine.createSpy();
    public startUpdate = jasmine.createSpy();
    public updatePost = jasmine.createSpy();
    public deletePost = jasmine.createSpy();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent],
      providers: [provideMockStore(), NgForm, { provide: MainService, useClass: MainServiceMock }],
      imports: [FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    mainService = TestBed.get(MainService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onPostFormSubmit', () => {
    it('should call create function', async () => {
      component.onPostFormSubmit(testForm);

      expect(mainService.createPost).toHaveBeenCalled();
    });

    it('should call edit function on edit', async () => {
      component.isEditing = true;
      component.onPostFormSubmit(testForm);

      expect(mainService.updatePost).toHaveBeenCalled();
    });
  });

  describe('onEditPostClick', () => {
    it('should set form values when ediit is pressed', async () => {
      const post: Post = { title: 'rock', message: 'solid' };
      const testForm = <NgForm>{
        controls: {},
      };
      testForm.controls.title = new FormControl('');
      testForm.controls.message = new FormControl('');

      expect(testForm.controls.title.value).not.toEqual(post.title);
      expect(testForm.controls.message.value).not.toEqual(post.message);

      component.onEditPostClick(post, testForm);

      expect(testForm.controls.title.value).toEqual(post.title);
      expect(testForm.controls.message.value).toEqual(post.message);
    });
  });
});
