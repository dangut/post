import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './store/post.reducer';

export const postFeatureSelector = createFeatureSelector<State>('post');

export const postErrorSelector = createSelector(postFeatureSelector, post => post && post.errorMessage);

export const postLoadingSelector = createSelector(postFeatureSelector, post => post && post.loading);

export const postEditingSelector = createSelector(postFeatureSelector, post => post && post.editing);

export const postsSelector = createSelector(postFeatureSelector, post => post && post.posts);
