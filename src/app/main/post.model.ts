export interface Post {
  title: string;
  message: string;
  id?: string;
}
