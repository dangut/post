import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromPost from '../main/store/post.reducer';

export interface State {
  post: fromPost.State;
}

export const reducers: ActionReducerMap<State> = {
  post: fromPost.postReducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
