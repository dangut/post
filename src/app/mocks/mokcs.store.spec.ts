import { BehaviorSubject, of } from 'rxjs';

export class MockStore<T> extends BehaviorSubject<T> {
  dispatch = jasmine.createSpy('MockStore::dispatch');
}
